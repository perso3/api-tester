let fetchMethod = document.getElementById('fetch-method');
let fetchUrl = document.getElementById('fetch-url');
let fetchButton = document.getElementById('fetch-button');
let fetchHeader = document.getElementById('fetch-header');

let outputResponse = document.getElementById('output-response');
let outputHeader = document.getElementById('output-header');
let outputElement = document.getElementById('output-body');
let outputError = document.getElementById('output-error');
let library = {};

library.json = {
    replacer: function(match, pIndent, pKey, pVal, pEnd) {
        let key = '<span class=json-key>';
        let val = '<span class=json-value>';
        let str = '<span class=json-string>';
        let r = pIndent || '';
        if (pKey)
            r = r + key + pKey.replace(/[": ]/g, '') + '</span>: ';
        if (pVal)
            r = r + (pVal[0] === '"' ? str : val) + pVal + '</span>';
        return r + (pEnd || '');
    },
    prettyPrint: function(obj) {
        let jsonLine = /^( *)("[\w]+": )?("[^"]*"|[\w.+-]*)?([,[{])?$/mg;
        return JSON.stringify(obj, null, 3)
            .replace(/&/g, '&amp;').replace(/\\"/g, '&quot;')
            .replace(/</g, '&lt;').replace(/>/g, '&gt;')
            .replace(jsonLine, library.json.replacer);
    }
};

function getHeaders() {
    let headers = {};

    let headerText = fetchHeader.value;

    let headerArray = headerText.split('\n');

    for (let i = 0; i < headerArray.length; i++) {
        let headerLine = headerArray[i];
        let headerLineArray = headerLine.split(':');
        if (headerLineArray.length === 2) {
            let key = headerLineArray[0].trim();
            let value = headerLineArray[1].trim();
            headers[key] = value;
        }
    }
    return headers;
}

let clearOutput = function() {
    outputResponse.innerHTML = '\n\n';
    outputHeader.innerHTML = '\n\n';
    outputElement.innerHTML = '\n\n';
    outputError.innerHTML = '\n\n';
}

fetchButton.onclick = () => {
    try {
        clearOutput();
        let headers = getHeaders();
        let init = {
            method: fetchMethod.value,
            headers: headers,
            mode: 'cors',
            cache: 'default'
        }
        fetch(fetchUrl.value, init)
            .then(async response => {
                let result;
                if (response.headers.get('Content-Type').indexOf('application/json') !== -1) {
                    result = await response.json();
                    outputElement.innerHTML = library.json.prettyPrint(result);
                } else if (response.headers.get('Content-Type').indexOf('text/html') !== -1) {
                    result = await response.text();
                    let htmlResult = new DOMParser().parseFromString(result, 'text/html');
                    outputElement.innerText = htmlResult.documentElement.outerHTML;
                } else {
                    result = await response.text();
                    outputElement.innerText = result;
                }

                outputResponse.innerHTML = response.status + ' ' + response.statusText;
                outputResponse.innerHTML += '<br>' + response.url;
                outputResponse.innerHTML += '<br>' + response.type;

                outputHeader.innerHTML = '';
                for (let [key, value] of response.headers.entries()) {
                    outputHeader.innerHTML += `${key}: ${value}<br>`;
                }

                if (!response.ok) {
                    outputError.innerHTML = response.statusText;
                }
            })
            .catch(error => {
                outputError.innerHTML += error;
            });
    }
    catch (error) {
        outputError.innerHTML += error;
    }

};


['log','debug','info','warn','error'].forEach(function (verb) {
    console[verb] = (function (method, verb, log) {
        return function () {
            method.apply(console, arguments);
            log.innerHTML += verb + ': ' + Array.prototype.slice.call(arguments).join(' ');
        };
    })(console[verb], verb, outputError);
});